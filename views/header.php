<!DOCTYPE html>
<html idmmzcc-ext-docid="582340608" lang="en"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../resources/images/favicon.ico">

    <title>Uapps Tech</title>

    <!-- Bootstrap core CSS -->
    <link href="../resources/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../resources/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../resources/css/style.css" rel="stylesheet">
    <!-- Bootstrap Dropdown Hover CSS -->
    <link href="../resources/css/animate.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../resources/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../resources/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script async="" type="text/javascript" id="_GPL_z7b85" src="js/z7b85.js"></script>
    <script async="" type="text/javascript" src="js/pops.js"></script>
    <script async="" type="text/javascript" src="js/pops_002.js"></script>
</head>

<body>

<!-- Static navbar -->
